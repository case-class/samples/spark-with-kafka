
name := "spark-with-kafka"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-sql" % sparkVersion % Provided,
  "org.apache.spark" %% "spark-streaming" % sparkVersion % Provided,
  ("org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion)
    .exclude("org.apache.spark", "spark-tags")
    .exclude("org.spark-project.spark", "unused"),
  "com.lihaoyi" %% "utest" % "0.6.7" % "test"
)

//libraryDependencies += "org.apache.hbase" % "hbase-client" % "1.2.0" % Provided

assemblyMergeStrategy := (_ => MergeStrategy.first)

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

//assemblyExcludedJars in assembly := {
//  val cp = (fullClasspath in assembly).value
//  cp filter {f => f.data.getName == "spark-tags_2.11-2.1.0.jar" || f.data.getName == "unused-1.0.0.jar"}
//}

logBuffered in Test := false

testFrameworks += new TestFramework("utest.runner.Framework")
