package com.gl

import org.apache.spark.sql.SparkSession

object Functions {


  val ratingRange: Double => String = rating => {
    if(rating <= 2) "Low"
    else if(rating <= 3.5) "Medium"
    else "High"
  }

  def registerRatingRangeUdf(spark: SparkSession) = {
    import org.apache.spark.sql.functions.udf
    spark.udf.register("rating_range", udf(ratingRange))
  }


}
