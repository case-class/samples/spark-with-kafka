package com.gl

import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}
import org.apache.log4j.Logger
import org.apache.log4j.Level

object WordCountStream extends App {

  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)

  val spark = SparkSession.builder()
    .appName("wordcount stream app")
    .getOrCreate()


  import spark.implicits._

  val dfStream = spark
    .readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", "kafka:9092")
    .option("subscribe", "test")
    .load()

  val dfStringStream: Dataset[(String, String)] = dfStream.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
    .as[(String, String)]


  val wordsStream: DataFrame = dfStringStream
    .flatMap(_._2.split(" "))
    .toDF("word")

  val wordCountsStream: DataFrame = wordsStream.groupBy("word").count()

  val query = wordCountsStream.writeStream
    .outputMode("complete")
    .format("console")
    .start()

  query.awaitTermination()

}
