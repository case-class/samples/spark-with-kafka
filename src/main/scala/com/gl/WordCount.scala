package com.gl

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Dataset, Encoder}

object WordCount  {

  def processWordCount(lines: RDD[String])
                      (implicit encoderS: Encoder[String],
                       encoderSI: Encoder[(String, Int)]): RDD[String] = {
    val words = lines.flatMap(_.split(" "))

    val wordsCount: RDD[(String, Int)] =
      words.map(_ -> 1).reduceByKey(_ + _)

    wordsCount.map {
      case (word, count) => s"$word,$count"
    }
  }

}
