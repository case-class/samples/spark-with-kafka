package com.gl

import org.apache.spark.{HashPartitioner, RangePartitioner}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.storage.StorageLevel

object MoviesApp extends App {

  val spark = SparkSession.builder()
    .appName("movies app")
    .getOrCreate()

  import spark.implicits._
  import spark.sql

  val moviesDF = spark.read
    .option("header", "true")
    .option("deriveSchema", "true")
    .csv(args(1))

  val ratingsDF = spark.read
    .option("header", "true")
    .option("deriveSchema", "true")
    .csv(args(1))

  moviesDF.createOrReplaceTempView("movies")
  ratingsDF.createOrReplaceTempView("ratings")

  sql("select movies.movie")

  //----------------------

  val moviesRDD: RDD[(Int, String)] = spark.sparkContext.parallelize(
    Seq(1 -> "Smurfs", 2 -> "Tarzan"))
  val ratingsRDD: RDD[(Int, Double)] = spark.sparkContext.parallelize(
    Seq(1 -> 1.0, 1 -> 1.5, 2 -> 4.0, 2 -> 3.5))

  val otherRDD: RDD[Int] = spark.sparkContext.parallelize(Seq(1, 2, 3))

  val partitioner = new HashPartitioner(100)

  moviesRDD.partitionBy(partitioner)
  ratingsRDD.partitionBy(partitioner)
  moviesDF.cache()

}
