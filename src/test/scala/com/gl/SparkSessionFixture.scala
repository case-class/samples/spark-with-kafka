package com.gl

import org.apache.spark.sql.SparkSession

trait SparkSessionFixture {

  val spark = SparkSession.builder().master("local[*]").getOrCreate()


}
