# Build and run Spark Streaming job

### Build

```bash
sbt assembly
```

The assembly jar should be created and stored in `./target/scala-2.11` folder.

### Start Spark and Kafka containers

```bash
docker-compose up --force-recreate
```

### Run Spark job in a container

```bash
docker exec -it spark-master /bin/bash
cd /spark-job
/spark/bin/spark-submit --class com.gl.WordCountStream spark-with-kafka-assembly-0.1.jar
```

### Publish some messages to Kafka topic

```bash
docker exec -it kafka /bin/bash
kafka-console-producer.sh --broker-list kafka:9092 --topic test
```
